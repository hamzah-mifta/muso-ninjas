import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth' 
import 'firebase/storage'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC1cb5BBqWZCQba5c5XORK2_FicsK-mFew",
  authDomain: "muso-ninja-7ec88.firebaseapp.com",
  projectId: "muso-ninja-7ec88",
  storageBucket: "muso-ninja-7ec88.appspot.com",
  messagingSenderId: "800803019635",
  appId: "1:800803019635:web:6eaadd8a6405afb2c50306",
  measurementId: "G-5943DJV4C9"
};

// init firebase
firebase.initializeApp(firebaseConfig)

// init services
const projectFirestore = firebase.firestore()
const projectAuth = firebase.auth()
const projectStorage = firebase.storage()

// generate timestamp
const timestamp = firebase.firestore.FieldValue.serverTimestamp

export { projectFirestore, projectAuth, projectStorage, timestamp }