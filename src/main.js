import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { projectAuth } from './firebase/config'

// global style
import './assets/main.css'

// initial app variables is null
let app

// execute every state change
projectAuth.onAuthStateChanged(() => {
  // only execute when app value is null
  if (!app) {
    app = createApp(App).use(router).mount('#app')
  }
})

